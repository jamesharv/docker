#!/bin/bash
set -xe

if [ ! -d "/mnt/solr" ]; then
  cp -r /opt/solr/multicore /mnt/solr
  rm -rf /mnt/solr/apachesolr
  rm -rf /mnt/solr/search_api
fi

for core in "`env | grep CORE_ | cut -d = -f 2-`";
do
  if [ -n "$core" ]; then
    NAME=`echo $core | cut -d : -f 1`
    TYPE=`echo $core | cut -d : -f 2`

    if [ ! -d "/mnt/solr/$NAME" ]; then
      cp -r /opt/solr/multicore/$TYPE /mnt/solr/$NAME
      sed -i "s@<!-- core -->@<core name=\"$NAME\" instanceDir=\"$NAME\" /><!-- core -->@g" /mnt/solr/solr.xml
    fi
  fi
done

java -jar /opt/solr/start.jar -Dsolr.solr.home=/mnt/solr
